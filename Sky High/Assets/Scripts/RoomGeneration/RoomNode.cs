﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RoomNode : MonoBehaviour
{
    public class NodeCost : IComparable
    {
        public Coordinates coordinates;
        //Total cost so far for the node
        public float gCost;
        //Estimated cost from this node to the goal node
        public float hCost;
        //parent node when it comes to finding paths
        public NodeCost parent;


        public NodeCost(Coordinates incomingCoordinates)
        {
            coordinates = incomingCoordinates;
            hCost = 0.0f;
            gCost = 1.0f;
            parent = null;
        }

        //clears the cost back to the original values
        public void Clear()
        {
            hCost = 0.0f;
            gCost = 1.0f;
            parent = null;
        }

        //IComparable Interface method implementation
        public int CompareTo(object obj)
        {
            NodeCost node = (NodeCost)obj;
            if (hCost < node.hCost)
            {
                return -1;
            }
            if (hCost > node.hCost)
            {
                return 1;
            }
            return 0;
        }
    }

    public RoomType roomType;
    public NodeCost cost;
    public Coordinates coordinates;
    public Dictionary<Direction, WallNode> WallConnector;
    public GameObject miniMapIcon;
    public GameObject roomLights;
    public List<GameObject> roomEnemies;
    public List<GameObject> roomItems;

    public bool roomDiscovered;
    public bool roomActive;
    public bool roomCleared;

    public void Initilize(Coordinates coordinatesInput, RoomType roomTypeInput)
    {
        coordinates = coordinatesInput;
        roomType = roomTypeInput;
        WallConnector = new Dictionary<Direction, WallNode>();
        cost = new NodeCost(coordinatesInput);
        miniMapIcon = gameObject.transform.GetChild(0).gameObject;
        roomLights = gameObject.transform.GetChild(1).gameObject;
        roomDiscovered = false;
        MiniMapIconSetActive(false);
        DeactivateRoom();
        roomCleared = false;
        roomEnemies = new List<GameObject>();
        roomItems = new List<GameObject>();
    }

    //activates the room so that the player is able to interact and see what is in it
    public void ActivateRoom()
    {
        roomActive = true;

        //if the room has not already been discovered, it will now be on the minimap
        if (!roomDiscovered)
        {
            roomDiscovered = true;
            MiniMapIconSetActive(true);
        }

        roomLights.SetActive(true);
        SetListActive(roomEnemies, true);
        SetListActive(roomItems, true);

        //may end up moving this method to the specific enemies that get defeated
        CheckIfRoomCleared();
    }

    //deactiavtes the room so that the player cannnot see or interact with the object in it
    public void DeactivateRoom()
    {
        //set all objects in the room as inactive
        roomActive = false;
        roomLights.SetActive(false);
        SetListActive(roomEnemies, false);
        SetListActive(roomItems, false);
    }


    //checks to see if there is anymore enemies in the room
    public void CheckIfRoomCleared()
    {
        if (roomEnemies.Count == 0)
        {
            SetRoomAsCleared();
        }
        else
            roomCleared = false;
    }

    //sets the minimap icon to be true or false depending on the argument
    public void MiniMapIconSetActive(bool activator)
    {
        miniMapIcon.SetActive(activator);
        foreach (KeyValuePair<Direction, WallNode> connector in WallConnector)
        {
            connector.Value.MiniMapIconSetActive(activator);
        }
    }

    //sets all objects in the passed in list as active or inactive
    void SetListActive(List<GameObject> objectList, bool activator)
    {
        foreach (GameObject obj in objectList)
        {
            obj.SetActive(activator);
        }
    }

    //creates a wall at the space specified, returns false if a wall cannot be generated
    public bool GenerateWall(Direction direction, WallNode wallnode)
    {
        if (WallConnector.ContainsKey(direction))
        {
            if (WallConnector[direction].wallType == WallType.WALL)
            {
                Debug.Log("Replacing wall at " + coordinates.ToString() +
                    "\nin the direction:" + direction.ToString() +
                    "\nwith the type of: " + wallnode.wallType.ToString());

                WallNode nodeBeingRemoved = WallConnector[direction];
                WallConnector.Remove(direction);
                Destroy(nodeBeingRemoved.gameObject);
                return true;
            }
            else
            {
                Debug.Log("ERROR: wall at " + coordinates.ToString() +
                        "\nin the direction:" + direction.ToString() +
                        "\nwallNode already exists cannot replaced");
                return false;
            }
        }

        //add the wall connector to the room
        WallConnector.Add(direction, wallnode);
        return true;
    }


    //the room is cleared. Unlock all of the doors and maybe give the player something
    void SetRoomAsCleared()
    {
        //if the room has not been cleared prior to checking
        if (!roomCleared)
        {
            //add in reward for clearing room here

            switch (roomType)
            {
                case RoomType.NORMAL:
                    //stuff
                    break;
                case RoomType.START:
                    //nothing happens here... its the start room
                    break;
                case RoomType.ITEM:
                    //you get an item or already got one?... I dont think enemies will be in this room
                    break;
                case RoomType.SHOP:
                    //you get to shop... i dont think enemies will be in this room either
                    break;
                case RoomType.BOSS:
                    //you get an item for defeating the boss... creates a portal for the next level
                    CreatePortalToNextFloor();
                    break;

                default:
                    Console.WriteLine("Unknown Room Type:" + roomType.ToString() + "room set as cleared");
                    break;
            }
            roomCleared = true;
        }
        UnlockBarredDoors();
    }

    //unlocks all barred doors in the room
    void UnlockBarredDoors()
    {
        foreach (KeyValuePair<Direction, WallNode> connector in WallConnector)
        {
            connector.Value.UnlockDoor(WallType.BARS);
        }
    }

    //create a portal in the room and add it to the list of items 
    void CreatePortalToNextFloor()
    {
        roomItems.Add(Instantiate(PrefabContainer.instance.portal, transform));
    }

    //script that generates items for a room
    void GenerateItems()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

    //script that generates NPCs for the room
    void GenerateNPCs()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

}
