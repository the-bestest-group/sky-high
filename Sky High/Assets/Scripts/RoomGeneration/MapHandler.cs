﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour
{
    public int floorNumber;
    Floor currentFloor;
    [HideInInspector] public GameObject currentRoom;

    public void Initilize()
    {
        floorNumber = 0;
        currentFloor = new Floor();
        CreateNewFloor(5, 5, Random.Range(3, 5), 4, PrefabContainer.instance.rooms, PrefabContainer.instance.walls);
    }

    //creates a new floor with the specified statistics
    public void CreateNewFloor(int maxRoomsX, int maxRoomsY, int numberOfRooms, int numberOfSpecialRooms, 
        List<GameObject> floorRoomPrefabs, List<GameObject> floorWallPrefabs)
    {
        floorNumber++;
        //create a floor layout
        currentFloor.Initilize(maxRoomsX, maxRoomsY, numberOfRooms,
            numberOfSpecialRooms, floorRoomPrefabs, floorWallPrefabs);
        //assign the start room as the current room
        currentRoom = currentFloor.GetRoomObject(RoomType.START);
        currentRoom.GetComponent<RoomNode>().ActivateRoom();
    }

    //transition the player from one room to another
    public void RoomTransition(WallNode wallNodeDestination)
    {
        GameHandler.instance.RoomTransition(wallNodeDestination);
    }

    //destroys all rooms in the current floor and assigns the maphandler a new floor
    public void DestroyCurrentFloor()
    {
        currentRoom = null;
        currentFloor.DestroyAllRooms();
        currentFloor = new Floor();
    }
}
