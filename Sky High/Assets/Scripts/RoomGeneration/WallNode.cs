﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallNode : MonoBehaviour
{
    public WallType wallType;
    public WallNode wallLink;
    [HideInInspector] public GameObject miniMapIcon;
    bool isLocked = false;

    //initilize the wall node if there is not already a walltype declared
    public void Initilize(WallType wallTypeInput)
    {
        wallType = wallTypeInput;

        if (wallTypeInput == WallType.BARS)
            isLocked = true;

        miniMapIcon = gameObject.transform.GetChild(0).gameObject;
        MiniMapIconSetActive(false);
    }

    //sets the minimap icon to be true or false depending on the argument
    public void MiniMapIconSetActive(bool activator)
    {
        miniMapIcon.SetActive(activator);
    }

    //links this node and another wall node together
    public void WallLink(WallNode linkNode)
    {
        wallLink = linkNode;

        //ensure we do not go into an endless loop function by adding an if statment
        if (linkNode.wallLink == null)
            linkNode.WallLink(this);
    }

    public void WallLink(GameObject linkNodeObject)
    {
        wallLink = linkNodeObject.GetComponent<WallNode>();

        //ensure we do not go into an endless loop function by adding an if statment
        if(linkNodeObject.GetComponent<WallNode>().wallLink == null)
            linkNodeObject.GetComponent<WallNode>().WallLink(this);
    }

    //unlocks a specific doortype based on the input
    public void UnlockDoor(WallType unlockType)
    {
        if (unlockType == wallType)
        {
            isLocked = false;
            miniMapIcon.GetComponent<SpriteRenderer>().color = Color.black;
        }
    }

    private void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.CompareTag("Player"))
        {
            if(!isLocked)
                GameHandler.instance.RoomTransition(wallLink);
        }
    }

}
