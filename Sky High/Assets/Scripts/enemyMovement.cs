﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMovement : MonoBehaviour
{
    Transform player;               // Reference to the player's position.
    PlayerHandler playerH;      // Reference to the player's health.
    enemyHealth enemyHealth;        // Reference to this enemy's health.
    UnityEngine.AI.NavMeshAgent nav;               // Reference to the nav mesh agent.


    void Awake()
    {
        // Set up the references.
        player = GameObject.Find("Player(Clone)").transform;
        playerH = player.GetComponent<PlayerHandler>();
        enemyHealth = GetComponent<enemyHealth>();
        nav = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }


    void Update()
    {
        // If the enemy and the player have health left...
        if (enemyHealth.currentealth > 0 && playerH.hpCurrent > 0)
        {
            // ... set the destination of the nav mesh agent to the player.
            nav.SetDestination(playerH.GetPlayerPosition());
        }
        // Otherwise...
        else
        {
        // ... disable the nav mesh agent.
            nav.enabled = false;
        }
    }
}
