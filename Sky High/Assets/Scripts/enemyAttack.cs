﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAttack : MonoBehaviour
{
    public float timeBetweenAttacks = 1.5f;     // The time in seconds between each attack.
    public float attackDamage = 0.5f;               // The amount of health taken away per attack.


    Animator anim;                              // Reference to the animator component.
    GameObject player;                          // Reference to the player GameObject.
    PlayerHandler playerH;                  // Reference to the player's health.
    enemyHealth enemyH;                    // Reference to this enemy's health.
    public bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    float timer;                                // Timer for counting up to the next attack.


    void Awake()
    {
        // Setting up the references.
        player = GameObject.Find("Player(Clone)");
        playerH = player.GetComponent<PlayerHandler>();
        enemyH = GetComponent<enemyHealth>();
        anim = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        // If the entering collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is in range.
            playerInRange = true;
            anim.SetBool("playerInRange", true);
        }
    }


    void OnTriggerExit(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is no longer in range.
            playerInRange = false;
            anim.SetBool("playerInRange", false);
        }
    }

    void Update()
    {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && playerInRange && enemyH.currentealth > 0)
        {
            // ... attack.
            Attack();
        }

        // If the player has zero or less health...
        if (playerH.hpCurrent <= 0)
        {
            // ... tell the animator the player is dead.
            anim.SetBool("PlayerDead", true);
        }
    }


    public void Attack()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
        if (playerH.hpCurrent > 0)
        {
            // ... damage the player.
            playerH.TakeDamage(attackDamage);
        }
    }

    
}
