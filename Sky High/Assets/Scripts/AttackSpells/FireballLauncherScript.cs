﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballLauncherScript : MonoBehaviour
{
    // To use Launcher scripts, attach to an empty game object, and attach that to a player 
    // or a WeaponHolder object to use weapon switching with this.

    public float fireRate = 3;      // Shots per second
    public Transform firepoint;     // Where the projectiles will be fired from   
    public GameObject projectileToFire;
    //public AudioSource shotSFX;
    
    private float timeBtwShots;
    private float timer;

    // Set this to true to enable the burn damage effect
    public bool fireType = false;

    // Start is called before the first frame update
    void Start()
    {
        timeBtwShots = 1 / fireRate;
        if (fireType)
        {
            projectileToFire.GetComponent<FireballScript>().fireType = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButton("Fire1") && timer >= timeBtwShots)
        {
            FireProjectile();
            timer = 0;
        }
    }

    void FireProjectile()
    {
        Instantiate(projectileToFire, firepoint.position, firepoint.rotation);
    }
}
