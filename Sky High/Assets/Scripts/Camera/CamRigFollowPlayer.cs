﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamRigFollowPlayer : MonoBehaviour
{
    // Must be attached to empty game object somewhere in the scene, and the camera placed
    // under that object as a child.
    
    
    public Transform playerToFollow;    //Drag in the player that you want this rig to follow, or tag them as "Player"

    // Start is called before the first frame update
    void Start()
    {
        var p1 = GameObject.FindGameObjectWithTag("Player");
        if (p1)
        {
            playerToFollow = p1.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = playerToFollow.position;
    }
}
