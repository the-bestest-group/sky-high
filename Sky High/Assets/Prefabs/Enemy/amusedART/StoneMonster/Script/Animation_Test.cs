﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_Test : MonoBehaviour
{

	public const string IDLE	= "Anim_Idle";
	public const string RUN		= "Anim_Run";
	public const string ATTACK	= "Anim_Attack";
	public const string DAMAGE	= "Anim_Damage";
	public const string DEATH	= "Anim_Death";

	Animation anim;
    public float timeBetweenAttacks = 1.5f;     // The time in seconds between each attack.
    public float attackDamage = 0.5f;               // The amount of health taken away per attack.

    GameObject player;                          // Reference to the player GameObject.
    PlayerHandler playerH;                  // Reference to the player's health.
    enemyHealth enemyH;                    // Reference to this enemy's health.
    enemyAttack enemyA;
    public bool playerInRange;                         // Whether player is within the trigger collider and can be attacked.
    float timer;

    void Awake()
    {
        // Setting up the references.
        anim = GetComponent<Animation>();
        player = GameObject.Find("Player(Clone)");
        playerH = player.GetComponent<PlayerHandler>();
        enemyH = GetComponent<enemyHealth>();
        enemyA = GetComponent<enemyAttack>();
    }

    void OnTriggerEnter(Collider other)
    {
        // If the entering collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is in range.
            playerInRange = true;
        }
    }


    void OnTriggerExit(Collider other)
    {
        // If the exiting collider is the player...
        if (other.gameObject.tag == "Player")
        {
            // ... the player is no longer in range.
            playerInRange = false;
        }
    }

    public void Update()
    {
        // Add the time since Update was last called to the timer.
        timer += Time.deltaTime;

        // If the timer exceeds the time between attacks, the player is in range and this enemy is alive...
        if (timer >= timeBetweenAttacks && playerInRange && enemyH.currentealth > 0)
        {
            // ... attack.
            anim.Play(ATTACK);
            Attack();
        }

        // If the player has zero or less health...
        if (playerH.hpCurrent <= 0)
        {
            // ... tell the animator the player is dead.
            anim.Play(IDLE);
            IdleAni();
        }
    }

    public void Attack()
    {
        // Reset the timer.
        timer = 0f;

        // If the player has health to lose...
        if (playerH.hpCurrent > 0)
        {
            // ... damage the player.
            playerH.TakeDamage(attackDamage);
        }
    }

        public void IdleAni ()
    {
        anim.CrossFade (IDLE);
	}

	public void RunAni ()
    {
		anim.CrossFade (RUN);
	}

	public void AttackAni ()
    {
		anim.CrossFade (ATTACK);
	}

	public void DamageAni ()
    {
		anim.CrossFade (DAMAGE);
	}

	public void DeathAni ()
    {
		anim.CrossFade (DEATH);
	}
}
